<x-app-layout>
    <div class="container text-center py-12">
        <i class="fas fa-exclamation-triangle text-9xl text-Orange-700"></i>
        <h1 class="font bold text-lg">
            Pagína no encontrada
        </h1>
        <p class="py-6 font-semibold">Lo sentimos pero la pagina que busca no ha sido encontrada</p>
        <x-button-enlace class="animate-bounce" href="/">
            <span >
                <i class="fas fa-long-arrow-alt-left"></i> Continuar comprando
            </span>
        </x-button-enlace>
    </div>
</x-app-layout>
